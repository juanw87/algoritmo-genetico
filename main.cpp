#include "include/config.hpp"
#include "include/problem.hpp"
#include "include/selector.hpp"
#include "include/crossover.hpp"
#include "include/mutator.hpp"
#include "include/replacer.hpp"

int main(int argc, char **argv){
    printf("begin process ....\n");
    srand (time(NULL));
    int i=0;
    Configuration cfg;
    cfg.read("./config/ga.config");

    //Utils u;
    Problem p;    
    Selector s(cfg.pselection);
    Crossover c(cfg.crossover);
    Mutator m(cfg.mutation);
    Replacer r(cfg.npreplacement, cfg.npselection);
    Population *parents = (Population*) malloc(sizeof(Population));
    Population *offprings = (Population*) malloc(sizeof(Population));

    p.load("./FSSPinstances/tai_20_5_1.txt");
    
    p.initialize_parents(cfg.mu, parents);
    p.initialize_population(cfg.lambda, offprings);

    Estadistica::get_start_time();

    // u.print_population(&parents);
    // u.print_population(offprings);
    // printf("================================================================== \n");
    do{
        printf("i: %d\n", i);
        // printf("select \n");
        s.select(parents, offprings);
        // printf("crossover \n");
        c.crossover(cfg.pcrossover, offprings);
        // printf("mutate \n");
        m.mutate(cfg.pmutation, offprings);
        //printf("================================================================== \n");
        // u.print_population(offprings);
        //printf("================================================================== \n");
        // printf("evaluate_population \n");
        p.evaluate_population(offprings);
        // printf("replace \n");
        r.replace(parents, offprings, &p);
        i++;
        Estadistica::increment_total_i(i);
    }while(i<cfg.epoch);

    Estadistica::get_final_time();
    // u.print_population(parents);
    printf("..... process has finished \n");
    Estadistica::sumarize();
    
    free(parents);
    free(offprings);    
    //printf("================================================================== \n");
}
