#include "../include/population.hpp"

Population::Population(int _sizep, int _sizes){
    size = _sizep;
    individual = new Individual[size];

    for(int i=0; i<size; i++){
        individual[i] = Individual(_sizep);
    }
}

Population::~Population(){
    delete[] individual;
}