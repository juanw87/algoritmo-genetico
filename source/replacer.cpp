#include "../include/replacer.hpp"

Replacer::Replacer(int a, int b){
    switch(a){
        case 0: operatorr = &Replacer::mu; //si este es el reemplazo generacional habría que o ponerle un nombre asociodo o un comentario que diga es el reemplazo generacional
                break;
        case 1: operatorr = &Replacer::mupluslambda;//por lo que charlamos ayer esto sería (mu+lambda) y acá habría que cambiar el nombre para no confundirse con el métod de reemplazo (mu, lambda)
                break;
        default: break;
    }

    switch(b){
        case 0: operators = &Replacer::b_tournament;
                break;
        case 1: operators = &Replacer::roulette;
                break;
        default: break;
    }
}

void Replacer::replace(Population *parents, Population *offprings, Problem *p){
    // cout << "ln: 17" << endl;
    (this->*operatorr)(parents, offprings, p);
}

void Replacer::select(Population *parents, Individual *offpring){
    (this->*operators)(parents, offpring);
}

void Replacer::mu(Population *parents, Population *offprings, Problem *p){
    // cout << "replace: mu" << endl;
    for(int i=0; i<offprings->size; i++){
        select(parents, &(offprings->individual[i]));
    }    
}

void Replacer::mupluslambda(Population *parents, Population *offprings, Problem *p){
    Utils u;
    // printf("replace: mu+lambda\n");
    //int mlsize = parents->size + offprings->size;
    int size = parents->size;   
    int r = 0;
    Population *aux_pop = (Population*) malloc(sizeof(Population));

    p->initialize_population(size, aux_pop);    
    // u.print_population(aux_pop);
    for(int i=0; i<size; i++){
        r = rand() % (parents->size + offprings->size);
        // cout << "r: " << r << endl;
        if(r<size){
            // cout << "ln: 37 - Replace parents i: " << i << endl;
            select(parents, &(aux_pop->individual[i]));
        }
        else{
            // cout << "ln: 43 - Replace offprings i: " << i  << endl;
            select(offprings, &(aux_pop->individual[i]));
        }
    }
    // cout << "ln: 59 - out for" << endl;
    u.copy_population(parents, aux_pop);

    //u.print_population(aux_pop);

    for(int i=0; i<aux_pop->size; i++){
        free(aux_pop->individual[i].solution.chromosome);
    }
    free(aux_pop->individual);
    free(aux_pop);
}

double* Replacer::asign_probability(Population *population){
    double global_fitness = 0;
    int size = population->size;
    double previous_fitness = 0.0;
    double *relative_fitness = (double*)calloc(size, sizeof(double));

    for (int i=0; i<size;i++){
        if(population->individual[i].fitness != 0){
            relative_fitness[i] += 1 / (double)population->individual[i].fitness;
        }
        global_fitness += relative_fitness[i];
    }

    for (int i=0;i<size;i++)
	{
		relative_fitness[i] = (relative_fitness[i] / global_fitness) + previous_fitness;
		previous_fitness = relative_fitness[i];
	}
    return relative_fitness;
}

void Replacer::b_tournament(Population *parents, Individual *offpring){
    Utils u;
    int r1 = 0;
    int r2 = 0;
    // cout << "ln: 96 - binary tournament" << endl;
    r1 = rand() % parents->size;
    do{
        r2 = rand() % parents->size;
    }while(r1 == r2);

    if(parents->individual[r1].fitness < parents->individual[r2].fitness){
        // cout << "ln: 103 - select parent 1 r1: " << r1 << endl;
        u.copy_individual(&(parents->individual[r1]), offpring);
    }
    else{
        // cout << "ln: 107 - select parent 2 r2: " << r2 << endl;
        u.copy_individual(&(parents->individual[r2]), offpring);
    }
    // cout << "ln: 110 - end binary tournament" << endl;
}

void Replacer::roulette(Population *parents, Individual *offpring){
    Utils u;
    double r = 0.0;
	int i = 0;
    double *relative_fitness;
    relative_fitness = asign_probability(parents);

    r = u.randd(0, 1);
    while (r > relative_fitness[++i]);
    //tomo i-1 porque al salir del bucle i queda pasado 1 unidad
    u.copy_individual(&(parents->individual[i-1]), offpring);
    i=0;
    
    free(relative_fitness);
}
