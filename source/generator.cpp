#include "../include/generator.hpp"

/*Generator::Generator(int n){
  switch(n){
    case 0: generate = &Generator::probabilistic;
      break;
    case 1: generate = &Generator::greedy;
    default: generate = &Generator::random;
      break;
  }
}*/

void Generator::random(Solution s){
    Utils u;
    for(int i=0; i<s.size; i++){
        s.chromosome[i] = i;
    }
    u.permute(s);
}

void Generator::probabilistic(){}

void Generator::greedy(){}
