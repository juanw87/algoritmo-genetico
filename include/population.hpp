/*
    Esta clase contiene rodas las funciones que intervienen en el remplazo generacional

    sumario:

*/

#ifndef POPULATION_HPP
#define POPULATION_HPP

#include "individualclass.hpp";

class Population{
private:
    Individual *individual;
    int size;
public:
  Population(int, int);
  ~Population();
};

#endif