/*
    Esta clase contiene rodas las funciones relacionadas con los cálculos estadísticos del algoritmo

    sumario:
      CX -> cycle crossover
      OX -> order crossover
      PMX -> partial mapped crossover
*/

#ifndef ESTADISTICA_HPP
#define ESTADISTICA_HPP

#include <fstream>
#include <iostream>
#include <stdlib.h>
#include "utils.hpp"

using namespace std; 

class Estadistica{
private:
    //estadistica de la población
    static int population_size;
    static double lower_bound;
    static double upper_bound;

    //estadisticas de fitness
    static double avg_fit;
    static double avg_init_fit;
    static double best_init;
    static double best_fit;
    static double total_fit;
    static float _gap;
    static float _agap;

    //estadisticas de iteraciones
    static int best_i;
    static int total_i;
    static int pos_best_sol;

    //estadisticas de tiempo
    static clock_t best_time;
    static clock_t start_time;
    static clock_t final_time;
    
    //estaditicas de evaluaciones
    static int total_eval;
    static int best_eval;
public:
    static void set_avg_fit();
    static void set_avg_init_fit();
    static void set_best_init(double);
    static void set_total_fit(double);
    static void increment_total_i(int);
    static clock_t get_final_time();
    static clock_t get_start_time();
    static void increment_total_eval();
    static void set_population_size(int);
    static void set_upper_bound(double);
    static void set_lower_bound(double);
    static void set_best_values(Individual*, int);
    static void gap();
    static void agap();
    static int get_total_i();
    static int get_total_eval();
    static double get_avg_fit();
    static double get_total_fit();
    static clock_t get_time();
    static void partial_sumarize();
    static void sumarize();
    static void header();
};

#endif
