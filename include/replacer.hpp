/*
    Esta clase contiene rodas las funciones que intervienen en el remplazo generacional

    sumario:

*/

#ifndef REPLACER_HPP
#define REPLACER_HPP

#include "selector.hpp"
#include "problem.hpp"

class Replacer{
private:
    void mu (Population*, Population*, Problem*);
    void mupluslambda (Population*, Population*, Problem*);
    void (Replacer::*operatorr)(Population*, Population*, Problem*);
    void (Replacer::*operators)(Population*, Individual*);
    double* asign_probability(Population*);
    void b_tournament(Population*, Individual*);
    void roulette(Population*, Individual*);
public:
  Replacer(int, int);
  void replace(Population*, Population*, Problem*);
  void select(Population*, Individual*);
};

#endif
