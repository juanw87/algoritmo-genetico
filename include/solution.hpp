/*
    Esta clase contiene rodas las funciones que intervienen en el remplazo generacional

    sumario:

*/

#ifndef SOLUTION_HPP
#define SOLUTION_HPP

class Solution{
private:
    int* chromosome;
    int size;
public:
  Solution();
  Solution(int);
  ~Solution();
};

#endif