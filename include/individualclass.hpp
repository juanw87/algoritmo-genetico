/*
    Esta clase contiene rodas las funciones que intervienen en el remplazo generacional

    sumario:

*/

#ifndef INDIVIDUAL_HPP
#define INDIVIDUAL_HPP

#include "solution.hpp"

class Individual{
private:
    Solution solution;
    double fitness;
    int updated;
public:
  Individual();
  Individual(int);
  ~Individual();
};

#endif