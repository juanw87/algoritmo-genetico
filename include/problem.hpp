/*
    Esta clase contiene todas las funciones generadoras de la problación inicial

    sumario:
      random -> genera una población de forma pseudoaleatoria
      probabilistic -> genera una problación seleccionando los individuos de acuerdo a una probabilidad
      greedy -> genera una población seleccionando los individuos de tal forma de lograr la mejor solución inicial
*/

#ifndef PROBLEM_HPP
#define PROBLEM_HPP

#include <fstream>
#include <iostream>
#include "utils.hpp"
#include "generator.hpp"
#include "estadistica.hpp"

using namespace std;

class Problem{
private:
    int msize;
    int jsize;
    int upperBound;
    int lowerBound;
    int **jobsTimes;
public:
    void load(string);
    void initialize_parents(int, Population*);
    void initialize_population(int, Population*);
    void delete_population(Population*);
    void evaluate(Individual*);
    void evaluate_population(Population*);
};

#endif
