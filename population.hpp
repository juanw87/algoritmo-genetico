/*********************************************************************************
* Population.h compendia definiciones de tipos y variables globales	     *
* Gabriela Minetti									     *
* Julio de 2012									     *
**********************************************************************************/


#ifndef POPULATION_HPP
#define POPULATION_HPP

typedef int * Solution;

struct individual{
 Solution sol;
 double fitness;
 int act;//Fitness = 0-actualizado 1-noActualizado
};

#endif
